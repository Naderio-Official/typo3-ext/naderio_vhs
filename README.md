# naderio_vhs

## Requirements

For now, this extension will only work on TYPO3 10.4.x with PHP 7.4. \
I have planned to release a Version with lower requirements, but I cannot promise a near release.

To install this package, you have to set the minimum stability in your root composer.json to:

    "minimum-stability": "beta",

This will be changed to stable, when the extension is tested enough.

## Description

Useful ViewHelpers, all of them working standalone, so it should be easily possible to use selected ViewHelpers in your own extensions.

__The documentation is work in progress and added when there is time :)__

## Running ddev

Just type `ddev start` (when DDEV is installed), to run the containers.  
To enter the web-container, type `ddev ssh`.

## CodeChecks

Inside the DDEV-container you can check the configured paths (`Classes/`) by running:

    ./vendor/bin/ecs check

to autofix found errors, type:

    ./vendor/bin/ecs check --fix

## Disclaimer

All ViewHelpers provided are mainly built for personal use by myself but I decided to publish them, so maybe you can have an advantage of them too. \
There is no guarantee for function and stability, all ViewHelper are tested with best knowledge and conscience by myself

## About

This extension is built by Thomas Anders, also known as `Naderio`. \
Feel free to contact me by mail: [me@naderio.de](mailto:me@naderio.de)

Extension links on:

* [Gitlab][gitlab-link]
* [Packagist][packagist-link]

[gitlab-link]: https://gitlab.com/Naderio-Official/typo3-ext/naderio_vhs

[packagist-link]: https://packagist.org/packages/naderio/naderio-vhs
