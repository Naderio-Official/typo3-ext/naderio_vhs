<?php

$EM_CONF['naderio_vhs'] = [
    'title' => 'Naderios ViewHelper Collection',
    'description' => 'Useful ViewHelpers',
    'author' => 'Thomas Anders',
    'author_email' => 'me@naderio.de',
    'state' => 'beta',
    'clearCacheOnLoad' => 1,
    'version' => '0.1.1',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.4.99'
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Naderio\\NaderioVhs\\' => 'Classes/'
        ]
    ]
];
