<?php

declare(strict_types=1);

namespace Naderio\NaderioVhs\ViewHelpers\Get;

use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use function count;

/**
 * Get related Fal Objects
 * Returns an [array] when [$multiple = true](default)
 * or only the first reference is found directly as [FileReference] when [$multiple = false].
 */
class FileReferencesViewHelper extends AbstractViewHelper
{
    protected FileRepository $fileRepository;

    public function __construct(
        FileRepository $fileRepository
    ) {
        $this->fileRepository = $fileRepository;
    }

    public function initializeArguments(): void
    {
        $this->registerArgument('table', 'string', 'Tablename (e.g. tt_content)', true);
        $this->registerArgument('contentUid', 'int', 'Uid of the element with attached FileReferences', true);
        $this->registerArgument('field', 'string', 'Fieldname of the FileReference (e.g. images or file)', true);
        $this->registerArgument('multiple', 'bool', 'Returns all FileReferences by default or only the first, if set to [false]', false, true);
    }

    /**
     * @return array|false
     */
    public function render()
    {
        $relatedFileReferences = $this->fileRepository->findByRelation(
            $this->arguments['table'],
            $this->arguments['field'],
            $this->arguments['contentUid']
        );

        if (! count($relatedFileReferences)) {
            return false;
        }

        if (count($relatedFileReferences) > 0) {
            if ($this->arguments['multiple'] === 0 ||
                $this->arguments['multiple'] === false ||
                $this->arguments['multiple'] === 'false') {
                return $relatedFileReferences[0];
            }

            return $relatedFileReferences;
        }

        return false;
    }
}
