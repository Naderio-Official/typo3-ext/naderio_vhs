<?php

declare(strict_types=1);

namespace Naderio\NaderioVhs\ViewHelpers\String;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use function gettype;

class ContainsViewHelper extends AbstractViewHelper
{
    public function initializeArguments(): void
    {
        $this->registerArgument('haystack', 'string', 'String that gets searched', true);
        $this->registerArgument('needle', 'string', 'string to search inside the haystack', true);
    }

    public function render(): bool
    {
        return gettype(strpos($this->arguments['haystack'], $this->arguments['needle'])) === 'integer';
    }
}
