<?php

declare(strict_types=1);

namespace Naderio\NaderioVhs\ViewHelpers\String\Sanitize;

use Closure;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

class TelLinkViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    public function initializeArguments(): void
    {
        $this->registerArgument('string', 'string', 'Phone-Number with specialchars', false);
    }

    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ): string {
        $string = $arguments['string'];

        if (empty($arguments['string'])) {
            $string = $renderChildrenClosure();
        }
        // Remove all invalid characters
        $sanitizedNumber = preg_replace('/[^0-9+]/', '', $string);

        // todo: Check and fix >all< available international prefixes, not only germany
        //region Check leading zero in international numberformat
        // check if there is +49 followed by a 0
        $posCheck = strpos($sanitizedNumber, '+490', 0);

        // Remove 0 from international format
        if (is_numeric($posCheck) && $posCheck === 0) {
            $sanitizedNumber = str_replace('+490', '+49', $sanitizedNumber);
        }
        //endregion

        return $sanitizedNumber;
    }
}
