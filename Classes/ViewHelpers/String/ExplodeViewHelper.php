<?php

declare(strict_types=1);

namespace Naderio\NaderioVhs\ViewHelpers\String;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Explodes a String with explode().
 * If $key is set (int), the specified numeric key is returned.
 */
class ExplodeViewHelper extends AbstractViewHelper
{
    public function initializeArguments(): void
    {
        $this->registerArgument('string', 'string', 'String to explode', true);
        $this->registerArgument('delimiter', 'string', 'delimiter', true);
        $this->registerArgument('key', 'int', 'key to return (begins on 0)', false);
    }

    /**
     * @return array|string
     */
    public function render()
    {
        if (is_numeric($this->arguments['key'])) {
            return explode($this->arguments['delimiter'], $this->arguments['string'])[$this->arguments['key']];
        }

        return explode($this->arguments['delimiter'], $this->arguments['string']);
    }
}
