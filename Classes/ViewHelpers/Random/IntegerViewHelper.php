<?php

declare(strict_types=1);

namespace Naderio\NaderioVhs\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Returns a random Integer from [min] to [max].
 * Default 0 - PHP_INT_MAX.
 */
class IntegerViewHelper extends AbstractViewHelper
{
    public function initializeArguments(): void
    {
        $this->registerArgument('min', 'integer', 'Minimum Value', false, 0);
        $this->registerArgument('max', 'integer', 'Maximum Value', false, PHP_INT_MAX);
    }

    public function render(): int
    {
        return random_int($this->arguments['min'], $this->arguments['max']);
    }
}
