<?php

declare(strict_types=1);

namespace Naderio\NaderioVhs\ViewHelpers\Object;

use Traversable, TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper, TYPO3Fluid\Fluid\Core\ViewHelper\Exception;
use function array_slice;
use function count;
use function gettype;

class ChunkViewHelper extends AbstractViewHelper
{
    public $escapeOutput = false;

    public function initializeArguments(): void
    {
        $this->registerArgument('objects', 'array', 'Items to chunk', true);
        $this->registerArgument('as', 'string', 'Variablename for the chunk', true, 'chunk');
        $this->registerArgument('size', 'int', 'Number of items per chunk', true, 1);
        $this->registerArgument('combined', 'bool', 'combined means, that all chunks are nested into one array. Use this, if you need to forEach the chunks themself', false, false);
    }

    public function render()
    {
        if (gettype($this->arguments['objects']) === 'object') {
            if (! $this->arguments['objects'] instanceof Traversable) {
                throw new Exception('Provided data is not supported. This works only witch arrays or objects.', 1591346332);
            }
        }

        try {
            $objects = iterator_to_array($this->arguments['objects']);
        } catch (Exception $e) {
            throw new Exception('Data cannot be parsed to array!', 1591346438);
        }

        $output = '';

        if ($this->arguments['combined']) {
            $chunks = [];
            for ($i = 0; $i <= count($objects); $i += $this->arguments['size']) {
                $items = array_slice($this->arguments['objects']->toArray(), $i, $this->arguments['size']);
                if (! empty($items)) {
                    $chunks[] = $items;
                }
            }

            $this->templateVariableContainer->add(
                $this->arguments['as'],
                $chunks
            );
            $output .= $this->renderChildren();
        } else {
            for ($i = 0; $i <= count($objects); $i += $this->arguments['size']) {
                $items = array_slice($this->arguments['objects']->toArray(), $i, $this->arguments['size']);
                if (! empty($items)) {
                    $this->templateVariableContainer->add(
                        $this->arguments['as'],
                        $items
                    );
                    $output .= $this->renderChildren();
                }
            }
        }

        return $output;
    }
}
