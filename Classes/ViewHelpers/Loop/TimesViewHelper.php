<?php

declare(strict_types=1);

namespace Naderio\NaderioVhs\ViewHelpers\Loop;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class TimesViewHelper extends AbstractViewHelper
{
    public $escapeOutput = false;

    public function initializeArguments(): void
    {
        $this->registerArgument('times', 'int', 'Does something x times', true);
    }

    public function render()
    {
        $output = '';
        $times = 1;
        while ($times <= $this->arguments['times']) {
            $output .= $this->renderChildren();
            $times++;
        }

        $this->renderChildren();

        return $output;
    }
}
