<?php

declare(strict_types=1);

namespace Naderio\NaderioVhs\ViewHelpers\Uri;

use Closure;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Fluid\ViewHelpers\Uri\ImageViewHelper as FluidImageViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

class ImageViewHelper extends FluidImageViewHelper
{
    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        try {
            $imageService = self::getImageService();
            $image = $imageService->getImage($arguments['src'], $arguments['image'], $arguments['treatIdAsReference']);

            // Check if file Exists
            if (! $image->getStorage()->getFile($image->getIdentifier())->getContents()) {
                $arguments['src'] = 'EXT:naderio_vhs/Resources/Public/Images/Placeholder/placeholder-general.png';
            }
        } catch (Exception $e) {
            switch ($e->getCode()) {
                case 1317178794: // No file reference (sys_file_reference) was found for given UID: XXX
                case 1476107295: // failed to open stream: No such file or directory
                case 1314516810: // Folder does not exist.
                case 1317178604: // No file found for given UID: xx.
                    $arguments['image'] = null;
                    $arguments['src'] = 'EXT:naderio_vhs/Resources/Public/Images/Placeholder/placeholder-general.png';
                    return parent::renderStatic($arguments, $renderChildrenClosure, $renderingContext);
            }
        }

        return parent::renderStatic($arguments, $renderChildrenClosure, $renderingContext);
    }
}
