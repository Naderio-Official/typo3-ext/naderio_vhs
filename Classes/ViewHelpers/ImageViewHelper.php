<?php

declare(strict_types=1);

namespace Naderio\NaderioVhs\ViewHelpers;

use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper as FluidImageViewHelper;

class ImageViewHelper extends FluidImageViewHelper
{
    public function render()
    {
        try {
            $image = $this->imageService
                ->getImage(
                    $this->arguments['src'],
                    $this->arguments['image'],
                    $this->arguments['treatIdAsReference']
                );
            // Check if file Exists
            if (empty($image->getStorage()->getFile($image->getIdentifier())->getContents())) {
                $this->setSrcToPlaceholder();
            }
        } catch (Exception $e) {
            switch ($e->getCode()) {
                case 1317178794: // No file reference (sys_file_reference) was found for given UID: XXX
                case 1476107295: // failed to open stream: No such file or directory
                case 1314516810: // Folder does not exist.
                case 1317178604: // No file found for given UID: xx.
                    $this->setSrcToPlaceholder();
                    return parent::render();
                default:
                    throw new Exception('Ein nicht näher spezifizierter Fehler ist aufgetreten');
            }
        }

        return parent::render();
    }

    private function setSrcToPlaceholder(): void
    {
        $this->arguments['image'] = null;
        $this->arguments['src'] = 'EXT:naderio_vhs/Resources/Public/Images/Placeholder/placeholder-general.png';
    }
}
