<?php

declare(strict_types=1);

namespace Naderio\NaderioVhs\ViewHelpers\Math;

use Closure;
use Exception;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * AdditionViewHelper
 * Returns the result of the addition of num1 + num2
 *  if a number is empty, 0 (zero) will be used instead.
 *  Usage:
 *   {c:math.addition(num1: 5, num2: 10)}
 *   {c:math.addition(num1: 5)}
 *  num1 can be added inline, so you can do:
 *   {number -> c:math.addition(num2: 10)}
 */
class AdditionViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    public function initializeArguments(): void
    {
        $this->registerArgument('num1', 'float', 'Summand 1', false, null);
        $this->registerArgument('num2', 'float', 'summand 2', false, null);
    }

    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $num1 = $arguments['num1'];
        $num2 = $arguments['num2'];

        if ($num1 === null) {
            $childrenClosure = $renderChildrenClosure();
            if (is_numeric($childrenClosure)) {
                $num1 = $childrenClosure;
            } elseif (empty($childrenClosure)) {
                $num1 = 0;
            } else {
                throw new Exception('num1 is no number');
            }
            if (empty($num1)) {
                $num1 = 0;
            }
        }

        if (empty($num2)) {
            $num2 = 0;
        }
        if (! is_numeric($num2)) {
            throw new Exception('num2 is no number');
        }

        return $num1 + $num2;
    }
}
